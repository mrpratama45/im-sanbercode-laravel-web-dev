<?php
require_once("animal.php");
require_once("Frog.php");
require_once("Ape.php");

            // Release 0
$sheep = new Animal("Shaun");

echo "<h1>Release 0</h1>" ;
echo "Name: ".$sheep->name . "<br>"; // "shaun"
echo "Legs: ".$sheep->legs . "<br>"; // 4
echo "Cold Blooded: ".$sheep->cold_blooded; // "no"
echo "<hr>";
            // Release 1
$kodok = new Frog("Buduk") ;

echo "<h1>Release 1</h1>" ;
echo "Name: ".$kodok->name . "<br>"; // "buduk"
echo "Legs: ".$kodok->legs . "<br>"; // 4
echo "Cold Blooded: ".$kodok->cold_blooded . "<br>"; // "no"
echo "Jump: ".$kodok->jump() ."<br><br>";; // "no"

$sungokong  = new Ape("Kera Sakti");

echo "Name: ".$sungokong ->name . "<br>"; // "kera sakti"
echo "Legs: ".$sungokong ->legs . "<br>"; // 2
echo "Cold Blooded: ".$sungokong ->cold_blooded . "<br>"; // "no"
echo "Yell: ".$sungokong ->yell(); // "no"
echo "<hr>";
?>