<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\dashboardControlle;
use App\Http\Controllers\tableController;
use App\Http\Controllers\dataTableController;
use App\Http\Controllers\castController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [dashboardControlle::class, "dashboard"]);
Route::get('/table', [tableController::class, "table"]);
Route::get('/dataTables', [dataTableController::class, "dataTable"]);

// C
Route::get('/cast/create', [castController::class, "create"]);
Route::post('/cast', [castController::class, "store"]);

// R
Route::get('/cast', [castController::class, "index"]);
Route::get('/cast/{cast_id}', [castController::class, "show"]);

// U
Route::get('/cast/{cast_id}/edit', [castController::class, "edit"]);
Route::put('/cast/{cast_id}', [castController::class, "update"]);

// D
Route::delete('/cast/{cast_id}', [castController::class, "destroy"]);