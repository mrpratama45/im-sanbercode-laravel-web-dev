<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;  //untuk fungsi data base

class castController extends Controller
{
    // C
    public function create (){

        return view ('crud.create');
    }

    public function store (request $request){
        // validasi not null
        $request->validate([
        'nama' => 'required|max:45',
        'umur' => 'required|max:11',
        'bio' => 'required'
    ]);
        DB::table('cast')->insert([
        'nama' => $request['nama'],
        'umur' => $request['umur'],
        'bio' => $request['bio']
        
    ]);
        // return view ('crud.cast');
        return redirect('/cast');
    }
    // akhir C

    // R
    public function index (){
        $cast = DB::table('cast')->get();
        // dd($cast);
        return view('crud.index', ['cast'=> $cast]);
    }

    public function show ($id){
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('crud.detail', ['cast'=> $cast]);
    }
    // akhir R
    
    // U
    public function edit ($id){
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('crud.edit', ['cast'=> $cast]);
        // return view ('crud.edit');
    }

    public function update(Request $request,$id){
        $request->validate([
        'nama' => 'required|max:45',
        'umur' => 'required|max:11',
        'bio' => 'required'
        ]);
        
        DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama'=> $request->nama,
                'umur'=> $request->umur,
                'bio'=> $request->bio
            ]);
            return redirect ('/cast');
    }

    // akhir U
    
    // D
     public function destroy($id){
        DB::table('cast')->where('id',$id)->delete();

            return redirect ('/cast');
    }

    // akhir D
}
