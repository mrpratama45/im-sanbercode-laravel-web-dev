@extends('main')

@section('judul')
    Halaman Cast
@endsection

@section('title-table')
    Daftar Cast
@endsection

@section('tittle')
    Cast
@endsection

@section('table-content')
<a href="/cast/create" class="nav font-weight-bold justify-content-end pr-5">Add</a>
    <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key => $valueCast)
    <tr>
      <td>{{$key +1}}</td>
      <td>{{$valueCast -> nama}}</td>
      <td>
        <form action="/cast/{{$valueCast->id}}" method='post'>
          @csrf
          @method('DELETE')
          <a href="/cast/{{$valueCast->id}}" class= "btn btn-info btn-sm">Detail</a>
          <a href="/cast/{{$valueCast->id}}/edit" class= "btn btn-warning btn-sm">Edit</a>
          <input type="submit" value='Delete' class= "btn btn-danger btn-sm">
        </form>
      </td>
    </tr>
    @empty
        <p>No users</p>
    @endforelse
  </tbody>
</table>
@endsection