@extends('main')

@section('judul')
    Halaman Edit Cast
@endsection

@section('title-table')
    Edit Cast
@endsection

@section('tittle')
    Edit Cast
@endsection

@section('table-content')
    <form action = "/cast/{{$cast->id}}" method="post">
        @csrf
        @method('PUT')
  <div class="form-group">
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <label>Nama Lengkap</label>
    <input type="text" name="nama" class="form-control" value='{{$cast -> nama}}' placeholder="Masukkan Nama Lengkap Anda">
  </div>
  <div class="form-group">
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <label>Umur</label>
    <input type="number" name="umur" min="1" onKeyPress="if(this.value.length==2) return false" value='{{$cast -> umur}}' class="form-control" placeholder="Masukan Umur Anda">
  </div>
  <div class="form-group">
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <label>Biodata</label>
     <textarea class="form-control" name="bio" rows="10" placeholder="Masukkan Biodata Anda">{{$cast -> bio}}</textarea>
  </div>
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection