@extends('main')

@section('judul')
    Halaman Cast
@endsection

@section('title-table')
    Daftar Cast
@endsection

@section('tittle')
    Cast
@endsection

@section('table-content')
    <form action = "/cast" method="post">
        @csrf
  <div class="form-group">
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <label>Nama Lengkap</label>
    <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama Lengkap Anda">{{$cast -> nama}}
  </div>
  <div class="form-group">
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <label>Umur</label>
    <input type="number" name="umur" min="1" onKeyPress="if(this.value.length==2) return false" class="form-control" placeholder="Masukan Umur Anda">{{$cast -> umur}}
  </div>
  <div class="form-group">
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <label>Biodata</label>
     <textarea class="form-control" name="bio" rows="10" placeholder="Masukkan Biodata Anda"></textarea>{{$cast -> bio}}
  </div>
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection