@extends('main')

@section('judul')
    Halaman Detail
@endsection

@section('title-table')
    Detail Cast
@endsection

@section('tittle')
    Detail Cast
@endsection

@section('table-content')
<form action ="/cast" method="post">
        @csrf
  <div class="form-group">
    <label>Nama Lengkap</label>
    <p>{{$cast -> nama}}</p>
  </div>
  <div class="form-group">
    <label>Umur</label>
    <p>{{$cast -> umur}}</p>
  </div>
  <div class="form-group">
   <label>Biodata</label>
     <p>{{$cast -> bio}}</p>
  </div>
  <a href="/cast" class="nav font-weight-bold justify-content-end pr-5">Kembali</a>
</form>
@endsection