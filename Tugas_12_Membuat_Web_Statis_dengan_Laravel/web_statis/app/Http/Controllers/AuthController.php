<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function welcome (request $request) {
        // dd($request->all());
        $firstName = $request['firstName'];
        $lastName = $request['lastName'];
        $fullName = $firstName . " " . $lastName;
        return view ("welcome", ['fullName' => $fullName]);
    }

    public function register (request $request) {
        return view ("register");
    }
}
