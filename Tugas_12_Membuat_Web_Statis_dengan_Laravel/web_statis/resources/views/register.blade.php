<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
      <label>First name:</label> <br />
      <input type="text" name="firstName" /> <br /><br />
      <label>Last name:</label> <br />
      <input type="text" name="lastName" /><br /><br />
      <label>Gender:</label> <br />
      <input type="radio" name="gender" value="male" />Male <br />
      <input type="radio" name="gender" value="female" />Female <br />
      <input type="radio" name="gender" value="other" />Other <br />
      <br />
      <label>Nationally:</label> <br />
      <select name="national" id="">
        <option value="ind">Indonesian</option>
        <option value="sgn">Singapura</option>
        <option value="mly">Malaysia</option>
      </select>
      <br /><br />
      <label>Language Spoken:</label> <br />
      <input type="checkbox" name="langSpoken1" value="Ind" />Bahasa Indonesia <br />
      <input type="checkbox" name="langSpoken2" value="Eng" />English <br />
      <input type="checkbox" name="langSpoken3" value="Oth" />Other <br /><br />
      <label>Bio:</label><br />
      <textarea name="deskrip_bio" id="" cols="50" rows="5"></textarea> <br />

      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
